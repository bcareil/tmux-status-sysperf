const std = @import("std");
const common = @import("common.zig");
const ansi = @import("color/ansi.zig");
const tmux = @import("color/tmux.zig");
const conf = @import("conf.zig");

const Buffer = common.Buffer;
const Conf = conf.Conf;

pub const Encoding = enum {
    AnsiEscapeCode,
    Tmux,
};

pub const Color = struct {
    r: f32,
    g: f32,
    b: f32,
};

pub const ColorInt = struct {
    r: u8,
    g: u8,
    b: u8,

    fn safe_cast(f: f32) u8 {
        var in = if (std.math.isNan(f)) 0.0 else f;
        in = std.math.clamp(in * 255.0, 0.0, 255.0);
        return @floatToInt(u8, in);
    }

    pub fn from_color(c: Color) ColorInt {
        return ColorInt{
            .r = safe_cast(c.r),
            .g = safe_cast(c.g),
            .b = safe_cast(c.b),
        };
    }
};

pub const Colors = struct {
    pub const White: Color = Color{ .r = 1.0, .g = 1.0, .b = 1.0 };
};

pub const GradientColorStopDef = struct {
    point: f32,
    color: Color,
};

pub const GradientDef = struct {
    color_stops: [12]GradientColorStopDef,
    num_stops: usize,

    const Self = @This();
    const Epsilon = 1e-3;

    pub fn init_gradient_2(start: Color, end: Color) GradientDef {
        var g = GradientDef{
            .color_stops = undefined,
            .num_stops = 0,
        };
        g.insert_color_stop(0, start) catch unreachable;
        g.insert_color_stop(1, end) catch unreachable;
        return g;
    }

    fn lower_bound(self: Self, point: f32) usize {
        for (self.color_stops[0..self.num_stops]) |sc, i| {
            if (point < sc.point) {
                return i;
            }
        } else {
            return self.num_stops;
        }
    }

    const FoundItem = struct {
        equal: bool,
        index: usize,
    };

    fn lower_bound_or_equal(self: Self, point: f32) FoundItem {
        var i: usize = 0;
        while (i < self.num_stops) {
            const cs = &self.color_stops[i];
            if (std.math.approxEq(f32, point, cs.point, Epsilon)) {
                return FoundItem{ .equal = true, .index = i };
            } else if (point < cs.point) {
                return .{ .equal = false, .index = i };
            }
            i += 1;
        }
        return FoundItem{ .equal = false, .index = self.num_stops };
    }

    pub fn insert_color_stop(self: *Self, point: f32, color: Color) !void {
        if (self.num_stops >= self.color_stops.len) {
            return error.NotEnoughSpace;
        }
        const item = self.lower_bound_or_equal(point);
        if (item.equal) {
            self.color_stops[item.index].point = point;
            self.color_stops[item.index].color = color;
            return;
        }
        const index = item.index;
        // make space for insertion
        std.mem.copyBackwards(GradientColorStopDef, self.color_stops[index + 1 .. self.num_stops + 1], self.color_stops[index..self.num_stops]);
        self.num_stops += 1;
        self.color_stops[index] = GradientColorStopDef{
            .point = point,
            .color = color,
        };
    }

    pub fn get_color(self: Self, point: f32) Color {
        if (self.num_stops == 0) {
            return Colors.White;
        } else if (self.num_stops == 1) {
            return self.color_stops[0].color;
        }
        var p = std.math.clamp(point, 0.0, 1.0);
        if (std.math.isNan(p)) {
            p = 0.0;
        }
        const next_index = self.lower_bound(point);
        const prev = &self.color_stops[next_index - 1];
        const next = &self.color_stops[next_index];
        const ratio = (point - prev.point) / (next.point - prev.point);
        return do_gradient_between(prev.color, next.color, ratio);
    }
};

pub const ColorDef = union(enum) {
    gradient: f32,
    color: Color,
    none: void,

    pub fn to_color(c: ColorDef, def: GradientDef) ?Color {
        return switch (c) {
            .gradient => |x_gradient| def.get_color(x_gradient),
            .color => |x_color| x_color,
            .none => null,
        };
    }
};

pub fn rgb_to_color(c: u32) Color {
    const b = c & 0xff;
    const g = (c >> 8) & 0xff;
    const r = (c >> 16) & 0xff;
    return Color{
        .r = @intToFloat(f32, r) / 255.0,
        .g = @intToFloat(f32, g) / 255.0,
        .b = @intToFloat(f32, b) / 255.0,
    };
}

pub fn lerp(a: f32, b: f32, ratio: f32) f32 {
    return a + (b - a) * ratio;
}

pub fn do_gradient_between(lhs: Color, rhs: Color, ratio: f32) Color {
    return Color{
        .r = lerp(lhs.r, rhs.r, ratio),
        .g = lerp(lhs.g, rhs.g, ratio),
        .b = lerp(lhs.b, rhs.b, ratio),
    };
}

pub fn do_gradient(def: GradientDef, ratio: f32) Color {
    return do_gradient_between(def.start, def.end, ratio);
}

pub fn set_colors(config: Conf, buffer: *Buffer, fg: ColorDef, bg: ColorDef) void {
    var fg_c = fg.to_color(config.gradient);
    var bg_c = bg.to_color(config.gradient);
    switch (config.encoding) {
        .AnsiEscapeCode => {
            ansi.set_colors(buffer, fg_c, bg_c);
        },
        .Tmux => {
            tmux.set_colors(buffer, fg_c, bg_c);
        },
    }
}

fn test_set_colors(expected: []const u8, fg: ?Color, bg: ?Color) !void {
    var b = Buffer{};
    tmux.set_colors(&b, fg, bg);
    if (!b.eql(expected)) {
        std.debug.print(
            \\
            \\Expected {s}
            \\Got      {s}
            \\
        , .{ expected, b.as_slice() });
        return error.TestFailed;
    }
}

test "weird colors" {
    try test_set_colors("#[fg=#ff00ff]#[bg=#000000]", Color{ .r = 1.2, .g = std.math.nan(f32), .b = std.math.inf(f32) }, Color{ .r = -std.math.inf(f32), .g = -3.0, .b = 0.0 });
}
