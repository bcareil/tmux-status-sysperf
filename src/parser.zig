const std = @import("std");
const meta = @import("meta.zig");

pub const TokenKind = enum {
    number,
    unsigned,
    string,
    identifier,
    composed_identifier,
    comment,
    equal,
};

pub const Token = struct {
    kind: TokenKind,
    token: []const u8,

    pub fn init_comment() Token {
        return Token{
            .kind = TokenKind.comment,
            .token = "",
        };
    }

    pub fn init_composed_identifier(token: []const u8) Token {
        return Token{
            .kind = TokenKind.composed_identifier,
            .token = token,
        };
    }

    pub fn init_equal() Token {
        return Token{
            .kind = TokenKind.equal,
            .token = "",
        };
    }

    pub fn init_identifier(token: []const u8) Token {
        return Token{
            .kind = TokenKind.identifier,
            .token = token,
        };
    }

    pub fn init_number(num: []const u8) Token {
        return Token{
            .kind = TokenKind.number,
            .token = num,
        };
    }

    pub fn init_string(str: []const u8) Token {
        return Token{
            .kind = TokenKind.string,
            .token = str,
        };
    }

    pub fn init_unsigned(unsigned: []const u8) Token {
        return Token{
            .kind = TokenKind.unsigned,
            .token = unsigned,
        };
    }

    pub fn eql(lhs: Token, rhs: Token) bool {
        if (lhs.kind != rhs.kind) {
            return false;
        }
        return std.mem.eql(u8, lhs.token, rhs.token);
    }
};

pub const Statement = struct {
    lhs: Token,
    rhs: Token,

    pub fn eql(lhs: Statement, rhs: Statement) bool {
        return lhs.lhs.eql(rhs.lhs) and lhs.rhs.eql(rhs.rhs);
    }
};

pub fn read_string(input: *[]const u8) !Token {
    if (input.len <= 0) {
        return error.InvalidCharacter;
    }
    if (input.*[0] != '"') {
        return error.InvalidCharacter;
    }
    if (input.len <= 1) {
        return error.MissingEndQuote;
    }

    var escaped = false;
    var idx = blk: for (input.*[1..]) |c, i| {
        switch (c) {
            '"' => {
                if (!escaped) {
                    break :blk i + 1;
                }
            },
            '\\' => {
                if (!escaped) {
                    escaped = true;
                    continue;
                }
            },
            else => {},
        }
        escaped = false;
    } else {
        return error.MissingEndQuote;
    };

    var tok = Token.init_string(input.*[1..idx]);
    input.* = input.*[idx + 1 ..];
    return tok;
}

pub fn read_num_sign(input: *[]const u8) void {
    switch (input.*[0]) {
        '-', '+' => {
            input.* = input.*[1..];
        },
        else => {},
    }
}

pub fn read_num_dec_point(input: *[]const u8) bool {
    if (input.len <= 0) {
        return false;
    }
    if (input.*[0] != '.') {
        return false;
    }
    input.* = input.*[1..];
    return true;
}

pub fn read_num_digit(input: *[]const u8) !void {
    if (input.len <= 0) {
        return error.InvalidToken;
    }
    switch (input.*[0]) {
        '0'...'9' => {
            input.* = input.*[1..];
        },
        else => {
            return error.InvalidCharacter;
        },
    }
}

pub fn read_num_int(input: *[]const u8) !void {
    try read_num_digit(input);
    while (input.len > 0) {
        read_num_digit(input) catch {
            return;
        };
    }
}

pub fn read_number(input: *[]const u8) !Token {
    var org: []const u8 = input.*;
    read_num_sign(input);
    try read_num_int(input);
    if (read_num_dec_point(input)) {
        try read_num_int(input);
    }
    return Token.init_number(org[0..(org.len - input.len)]);
}

pub fn read_hex_unsigned(input: *[]const u8) !Token {
    if (input.len < 3) {
        return error.InvalidToken;
    }
    if (!std.mem.eql(u8, input.*[0..2], "0x")) {
        return error.InvalidCharacter;
    }
    var slice = input.*[2..];
    var length = outer: for (slice) |c, i| {
        switch (c) {
            '0'...'9', 'a'...'f', 'A'...'F' => {
                if (i >= 8) {
                    // u32 exceeded
                    return error.InvalidToken;
                }
                // else keep going
            },
            'g'...'z', 'G'...'Z' => {
                // do not authorize any other letter
                return error.InvalidCharacter;
            },
            else => break :outer i,
        }
    } else outer: {
        break :outer slice.len;
    };
    length += 2; // include back "0x"
    var token: []const u8 = input.*[0..length];
    input.* = input.*[length..];
    return Token.init_unsigned(token);
}

pub fn read_any_number(input: *[]const u8) !Token {
    if (input.len >= 2 and input.*[0] == '0' and input.*[1] == 'x') {
        return read_hex_unsigned(input);
    } else {
        return read_number(input);
    }
}

pub fn read_identifier(input: *[]const u8) !Token {
    switch (input.*[0]) {
        'a'...'z', 'A'...'Z', '_' => {},
        else => {
            return error.InvalidCharacter;
        },
    }
    var idx = blk: for (input.*[1..]) |c, i| {
        switch (c) {
            'a'...'z', 'A'...'Z', '0'...'9', '_' => {},
            else => {
                break :blk i + 1;
            },
        }
    } else blk: {
        break :blk input.len;
    };
    var tok = Token.init_identifier(input.*[0..idx]);
    input.* = input.*[idx..];
    return tok;
}

pub fn skip_spaces(input: *[]const u8) void {
    var idx = blk: for (input.*) |c, i| {
        if (c != ' ' and c != '\t') {
            break :blk i;
        }
    } else blk: {
        break :blk input.len;
    };

    input.* = input.*[idx..];
}

pub fn read_token(input: *[]const u8) !?Token {
    skip_spaces(input);
    if (input.len <= 0) {
        return null;
    }
    switch (input.*[0]) {
        '=' => {
            input.* = input.*[1..];
            return Token.init_equal();
        },
        '#' => {
            input.* = input.*[input.len..];
            return Token.init_comment();
        },
        '"' => {
            return read_string(input) catch |err| return err;
        },
        '-', '0'...'9' => {
            return read_any_number(input) catch |err| return err;
        },
        else => {
            var cursor = input.*;
            while (true) {
                var ident = read_identifier(&cursor) catch |err| {
                    if (cursor.len == input.len) {
                        // first identifier to be read
                        return err;
                    } else {
                        // identifier expected after a .
                        return error.InvalidToken;
                    }
                };
                if (cursor.len > 0 and cursor[0] == '.') {
                    cursor = cursor[1..];
                    if (cursor.len == 0) {
                        return error.InvalidToken;
                    }
                    switch (cursor[0]) {
                        '0'...'9' => {
                            var num = read_number(&cursor) catch |err| return err;
                            // a num cannot be followed by an ident
                        },
                        else => {
                            continue;
                        },
                    }
                }

                var token_size = input.len - cursor.len;
                var token = input.*[0..token_size];
                input.* = cursor;
                if (token_size == ident.token.len) {
                    return ident;
                }
                return Token.init_composed_identifier(token);
            }
        },
    }
}

pub fn read_statement(input: []const u8) !?Statement {
    var slice = input[0..];
    var tok_lhs = (try read_token(&slice)) orelse return null;
    switch (tok_lhs.kind) {
        .comment => {
            return null;
        },
        .identifier, .composed_identifier => {
            var tok_eql = (try read_token(&slice)) orelse return error.InvalidToken;
            if (tok_eql.kind != .equal) {
                return error.UnexpectedToken;
            }
            var tok_rhs = (try read_token(&slice)) orelse return error.InvalidToken;
            switch (tok_rhs.kind) {
                .equal, .comment => {
                    return error.UnexpectedToken;
                },
                else => {
                    var tok_rem = try read_token(&slice);
                    if (tok_rem) |tok| {
                        if (tok.kind != .comment) {
                            return error.UnexpectedToken;
                        }
                    }
                    return Statement{
                        .lhs = tok_lhs,
                        .rhs = tok_rhs,
                    };
                },
            }
        },
        else => {
            return error.UnexpectedToken;
        },
    }
}

fn test_read_token(input: []const u8, expected: anyerror!?Token) !void {
    var slice = input[0..];
    var result = read_token(&slice);
    if (meta.eql(result, expected)) {
        return;
    }
    std.debug.print("Expected\n{}\nGot\n{}\n", .{ expected, result });
    return error.TestFailed;
}

fn test_read_statement(input: []const u8, expected: anyerror!?Statement) !void {
    var result = read_statement(input);
    if (meta.eql(result, expected)) {
        return;
    }
    std.debug.print("Expected\n{}\nGot\n{}\n", .{ expected, result });
    return error.TestFailed;
}

test "read identifier" {
    try test_read_token("abc", Token.init_identifier("abc"));
    try test_read_token("abc=efg", Token.init_identifier("abc"));
    try test_read_token("abc efg", Token.init_identifier("abc"));
    try test_read_token(" \tabc", Token.init_identifier("abc"));
}
test "read composed identifier" {
    try test_read_token(" \ta.b.c", Token.init_composed_identifier("a.b.c"));
    try test_read_token("a.b .c", Token.init_composed_identifier("a.b"));
    try test_read_token("a.b. c", error.InvalidToken);
}

test "read string" {
    try test_read_token("\"\"", Token.init_string(""));
    try test_read_token(" \"\" ", Token.init_string(""));
    try test_read_token("\"abc\"", Token.init_string("abc"));
    try test_read_token(" \"abc\"efg", Token.init_string("abc"));
    try test_read_token("\"a=\\\\b\\\"c\"", Token.init_string("a=\\\\b\\\"c"));

    try test_read_token("\"", error.MissingEndQuote);
    try test_read_token("\"#efg", error.MissingEndQuote);
}

test "read number" {
    try test_read_token("123", Token.init_number("123"));
    try test_read_token("-123", Token.init_number("-123"));
    try test_read_token("0.123", Token.init_number("0.123"));
    try test_read_token("-12.3", Token.init_number("-12.3"));

    try test_read_token("-", error.InvalidToken);
    try test_read_token("-a", error.InvalidCharacter);
    try test_read_token("-1.a", error.InvalidCharacter);
}

test "read statement" {
    var stmt = Statement{
        .lhs = Token.init_identifier("abc"),
        .rhs = Token.init_number("123"),
    };
    try test_read_statement("abc=123", stmt);
    try test_read_statement("abc =123", stmt);
    try test_read_statement("abc= 123", stmt);
    try test_read_statement("  abc = 123  ", stmt);
    try test_read_statement("  abc = 123  # blah 123 \"xyz", stmt);

    stmt = Statement{
        .lhs = Token.init_identifier("abc"),
        .rhs = Token.init_string("e#g"),
    };
    try test_read_statement("abc=\"e#g\"", stmt);

    stmt = Statement{
        .lhs = Token.init_composed_identifier("a.b.c"),
        .rhs = Token.init_number("123"),
    };
    try test_read_statement("a.b.c=123", stmt);
    try test_read_statement(" a.b.c=123", stmt);
    try test_read_statement(" a.b.c = 123", stmt);

    try test_read_statement("# abc = 123", null);
    try test_read_statement("  # abc = 123", null);
    try test_read_statement(" #\"x", null);

    try test_read_statement("abc#efg", error.UnexpectedToken);
    try test_read_statement("abc=#efg", error.UnexpectedToken);
}
