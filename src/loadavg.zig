const std = @import("std");
const common = @import("common.zig");
const color = @import("color.zig");

const loadavg = "/proc/loadavg";

const Measurment = common.Measurment;
const SmallBuffer = common.SmallBuffer;

pub fn read_1min_loadavg() !f32 {
    const open_ro = std.fs.File.OpenFlags{ .read = true };
    var loadavg_fd = try std.fs.openFileAbsolute(loadavg, open_ro);
    defer loadavg_fd.close();

    var buffer: [128]u8 = undefined;
    var read = try loadavg_fd.read(&buffer);
    for (buffer[0..read]) |char, i| {
        if (char == ' ') {
            read = i;
            break;
        }
    }

    return try std.fmt.parseFloat(f32, buffer[0..read]);
}

pub fn format_loadavg(out: *SmallBuffer, load: f32) void {
    var buf = std.fmt.bufPrint(&out.buffer, "{d:>4.2}", .{load}) catch unreachable;
    out.index = buf.len;
}

pub fn loadavg_1min() !Measurment {
    var measurment = Measurment.init();
    measurment.field = .load_average;
    const load = try read_1min_loadavg();
    format_loadavg(&measurment.text, load);
    const num_cpus = 4;
    const load_norm = load / num_cpus;
    measurment.coloring = color.ColorDef{ .gradient = load_norm };
    return measurment;
}
