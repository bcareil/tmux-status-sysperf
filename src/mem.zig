const std = @import("std");
const common = @import("common.zig");
const color = @import("color.zig");

const Measurment = common.Measurment;

const meminfo = "/proc/meminfo";

const LineBuffer = struct {
    buffer: [4096]u8 = undefined,
    used: usize = 0,
    next_line: usize = 0,
};

pub const MemoryInfo = struct {
    total: f32 = 0,
    free: f32 = 0,
    available: f32 = 0,
    buffers: f32 = 0,
    cached: f32 = 0,
};

const ReadLineError = error{
    EndOfFile,
    LineTooLong,
};

pub fn is_digit(c: u8) bool {
    return c >= '0' and c <= '9';
}

pub fn starts_with(str: []const u8, to_match: []const u8) bool {
    if (str.len < to_match.len) {
        return false;
    }
    for (str[0..to_match.len]) |c, i| {
        if (c != to_match[i]) {
            return false;
        }
    }
    return true;
}

pub fn parse_meminfo_line(line: []const u8) !f32 {
    if (line.len == 0) {
        return error.ParseError;
    }
    const end = line.len;
    var scale_index: ?usize = null;
    var scale: f32 = 0;
    var i = end;
    if (line[i - 1] == 'B') {
        // read the scale
        while (i > 0 and line[i - 1] != ' ') {
            i -= 1;
        }
        scale_index = i;
        // TODO: validate scale
        scale = 1024.0;

        // skip spaces between the number and the unit
        while (i > 0 and line[i - 1] == ' ') {
            i -= 1;
        }
    }

    const end_of_digit = i;
    if (!is_digit(line[i - 1])) {
        return error.ParseError;
    }

    while (i > 0 and is_digit(line[i - 1])) {
        i -= 1;
    }

    var num = try std.fmt.parseFloat(f32, line[i..end_of_digit]);
    return num * scale;
}

pub fn read_line(fd: *std.fs.File, buffer: *LineBuffer) ![]u8 {
    if (buffer.next_line < buffer.used) {
        // search for the next line in what we have already read
        for (buffer.buffer[buffer.next_line..buffer.used]) |c, i| {
            if (c == '\n') {
                const start = buffer.next_line;
                buffer.next_line = start + i + 1;
                return buffer.buffer[start..(start + i)];
            }
        }
    }
    // a '\n' was not encountered

    // copy the potentially truncated line to the start of the buffer
    for (buffer.buffer[buffer.next_line..buffer.used]) |c, i| {
        buffer.buffer[i] = c;
    }
    buffer.used = buffer.used - buffer.next_line;
    buffer.next_line = 0;

    var read = try fd.read(buffer.buffer[buffer.used..buffer.buffer.len]);
    if (read == 0) {
        // we reached EOF
        if (buffer.used == 0) {
            return error.EndOfFile;
        } else {
            buffer.next_line = buffer.used;
            return buffer.buffer[0..buffer.used];
        }
    } else {
        // search for a new line in the data we just read
        const iter_start = buffer.used;
        buffer.used += read;
        for (buffer.buffer[iter_start..buffer.used]) |c, i| {
            if (c == '\n') {
                buffer.next_line = iter_start + i + 1;
                return buffer.buffer[0..(iter_start + i)];
            }
        }
        return error.LineTooLong;
    }
}

pub fn get_meminfo() !MemoryInfo {
    const open_ro = std.fs.File.OpenFlags{ .read = true };
    var fd = try std.fs.openFileAbsolute(meminfo, open_ro);
    defer fd.close();

    var buffer = LineBuffer{};
    var info = MemoryInfo{};
    var fields_read: usize = 0;
    while (fields_read < 5) {
        var line = try read_line(&fd, &buffer);
        if (starts_with(line, "MemTotal")) {
            info.total = try parse_meminfo_line(line);
            fields_read += 1;
        } else if (starts_with(line, "MemFree")) {
            info.free = try parse_meminfo_line(line);
            fields_read += 1;
        } else if (starts_with(line, "MemAvailable")) {
            info.available = try parse_meminfo_line(line);
            fields_read += 1;
        } else if (starts_with(line, "Buffers")) {
            info.buffers = try parse_meminfo_line(line);
            fields_read += 1;
        } else if (starts_with(line, "Cached")) {
            info.cached = try parse_meminfo_line(line);
            fields_read += 1;
        }
    }
    return info;
}

pub fn get_pct_used() !Measurment {
    const mem = try get_meminfo();
    var pct_used = (mem.total - mem.available) / mem.total;
    var measurment: Measurment = .{
        .text = common.SmallBuffer{},
        .coloring = color.ColorDef{ .gradient = pct_used },
        .field = .ram,
    };
    var printed = std.fmt.bufPrint(&measurment.text.buffer, "{d:>3.0}%", .{100.0 * pct_used}) catch unreachable;
    measurment.text.index = printed.len;
    return measurment;
}
