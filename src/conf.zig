const std = @import("std");
const color = @import("color.zig");
const common = @import("common.zig");
const parser = @import("parser.zig");

const Allocator = std.mem.Allocator;

const SmallBuffer = common.SmallBuffer;
const Buffer = common.Buffer;

const config_filename = "tmux-status-sysperf.conf";
const config_paths = [_][:0]const u8{
    "$HOME/.",
    "$HOME/.config/tmux-status-sysperf/",
};

const KeyValue = struct {
    key: SmallBuffer,
    value: SmallBuffer,
};

const KeyValueList = std.ArrayList(KeyValue);

pub const Fields = enum {
    uptime,
    cpu,
    ram,
    load_average,
};

pub const NumFields = @typeInfo(Fields).Enum.fields.len;

pub const FieldConf = struct {
    symbol: SmallBuffer,
};

pub const Conf = struct {
    fields: [NumFields]FieldConf,
    separator: SmallBuffer,
    gradient: color.GradientDef,
    encoding: color.Encoding,
};

const Default = Conf{
    .fields = [_]FieldConf{FieldConf{ .symbol = SmallBuffer{} }} ** 4,
    .separator = init: {
        var b = SmallBuffer{
            .index = 0,
            .buffer = undefined,
        };
        const dflt = "";
        std.mem.copy(u8, &b.buffer, dflt);
        b.index = dflt.len;
        break :init b;
    },
    .gradient = color.GradientDef.init_gradient_2(
        color.rgb_to_color(0x3a467d),
        color.rgb_to_color(0xfc5444),
    ),
    .encoding = color.Encoding.Tmux,
};

fn guess_color_encoding(stream: std.fs.File) color.Encoding {
    return blk: {
        if (stream.isTty()) {
            break :blk color.Encoding.AnsiEscapeCode;
        } else {
            break :blk color.Encoding.Tmux;
        }
    };
}

const ValidIdentifiers = [_][]const u8{
    "separator",
};

fn validate_identifier(token: parser.Token) !void {
    switch (token.kind) {
        .identifier => {
            for (ValidIdentifiers) |expected_name| {
                if (std.mem.eql(u8, expected_name, token.token)) {
                    return;
                }
            }
        },
        .composed_identifier => {
            inline for (@typeInfo(Fields).Enum.fields) |f| {
                const expected = f.name ++ ".";
                const len = expected.len;
                if (token.token.len >= len and std.mem.eql(u8, expected, token.token[0..len])) {
                    return;
                }
            }
            const expected = "gradient.";
            const len = expected.len;
            if (token.token.len >= len and std.mem.eql(u8, expected, token.token[0..len])) {
                return;
            }
        },
        else => {},
    }
    return error.InvalidConfKey;
}

fn add_conf_entry(list: *KeyValueList, key: []const u8, value: []const u8) !void {
    for (list.items) |i| {
        if (i.key.eql(key)) {
            return error.DuplicatedConfKey;
        }
    }
    try list.append(KeyValue{
        .key = try SmallBuffer.from_slice(key),
        .value = try SmallBuffer.from_slice(value),
    });
}

fn read_conf_line(input: []const u8, list: *KeyValueList) !void {
    var stmt = try parser.read_statement(input);
    if (stmt) |stmt_v| {
        try validate_identifier(stmt_v.lhs);
        try add_conf_entry(list, stmt_v.lhs.token, stmt_v.rhs.token);
    }
}

fn consume_conf_line(input: *[]const u8, list: *KeyValueList) bool {
    var eol = std.mem.indexOf(u8, input.*, "\n") orelse return false;
    var line = input.*[0..eol];
    input.* = input.*[eol + 1 ..];
    read_conf_line(line, list) catch |err| {
        std.log.warn("Error reading configuration: {}\nLine: {s}\n", .{ err, line });
    };
    return true;
}

fn load_conf_file(conf_file: std.fs.File, alloc: *Allocator) !KeyValueList {
    var list = KeyValueList.init(alloc);
    var buffer = Buffer{
        .index = 0,
        .buffer = undefined,
    };
    while (true) {
        var read = try conf_file.read(buffer.buffer[buffer.index..]);
        buffer.index += read;
        if (buffer.index == 0) {
            break;
        }
        if (read == 0 and buffer.index < buffer.buffer.len and buffer.buffer[buffer.index - 1] != '\n') {
            // if the file is not ending with a \n, add it
            buffer.append("\n");
        }
        var remaining = buffer.buffer[0..buffer.index];
        while (consume_conf_line(&remaining, &list)) {}
        var consumed = buffer.index - remaining.len;
        if (consumed > 0) {
            // some stuff has been consumed by consume_conf_line
            std.mem.copy(u8, buffer.buffer[0..], buffer.buffer[consumed..buffer.index]);
            buffer.index -= consumed;
        } else {
            // likelly not a single \n in the whole buffer
            return error.LineTooLong;
        }
    }
    return list;
}

fn read_variable(input: *[]const u8) ![]const u8 {
    if (input.len < 2) {
        return error.InvalidToken;
    }
    if (input.*[0] != '$') {
        return error.InvalidCharacter;
    }
    input.* = input.*[1..];
    var tok = try parser.read_identifier(input);
    return tok.token;
}

fn expand_variable(buffer: []u8, name: []const u8) ![]u8 {
    var value = std.os.getenv(name) orelse return error.EnvVariableNotFound;
    if (value.len > buffer.len) {
        return error.NotEnoughSpace;
    }
    std.mem.copy(u8, buffer, value);
    return buffer[0..value.len];
}

fn expand_path(buffer: []u8, input: []const u8) ![]u8 {
    var j: usize = 0;
    var slice = input[0..];
    while (slice.len > 0) {
        switch (slice[0]) {
            '$' => {
                var var_name = try read_variable(&slice);
                var written = try expand_variable(buffer[j..], var_name);
                j += written.len;
            },
            else => |c| {
                if (j < buffer.len) {
                    buffer[j] = c;
                    j += 1;
                } else {
                    return error.NotEnoughSpace;
                }
                slice = slice[1..];
            },
        }
    }
    return buffer[0..j];
}

fn load_conf(alloc: *Allocator) KeyValueList {
    var path_buffer: [4096]u8 = undefined;
    for (config_paths) |path_tpl| {
        var written = expand_path(&path_buffer, path_tpl) catch |err| {
            std.debug.print("Error expanding {s}: {}\n", .{ path_tpl, err });
            continue;
        };
        var path_len = config_filename.len + written.len;
        if (path_len > path_buffer.len) {
            continue;
        }
        std.mem.copy(u8, path_buffer[written.len..], config_filename);
        var path = path_buffer[0..path_len];

        var file = std.fs.openFileAbsolute(path, .{ .read = true }) catch |err| {
            //std.debug.print("Cannot access {}: {}\n", .{ path, err });
            continue;
        };
        defer file.close();

        if (load_conf_file(file, alloc)) |list| {
            return list;
        } else |err| {
            std.debug.print("Error loading conf: {}\n", .{err});
            break;
        }
    }
    return KeyValueList.init(alloc);
}

fn eval_string(out: *SmallBuffer, str: []const u8) void {
    var slice = str[0..];
    var escaped = true;
    while (slice.len > 0) {
        if (escaped) {
            escaped = false;
            switch (slice[0]) {
                't' => out.append("\t"),
                'n' => out.append("\n"),
                else => |c| out.append(c),
            }
        } else {
            switch (slice[0]) {
                '\\' => escaped = true,
                else => |c| out.append(c),
            }
        }
        slice = slice[1..];
    }
}

fn eval_unsigned(token: []const u8) u32 {
    var res: u32 = 0;
    if (token.len < 2) {
        // should have been handled during parsing
        return 0;
    }
    return std.fmt.parseUnsigned(u32, token, 0) catch 0;
}

fn parse_field_conf_entry(conf: *FieldConf, pair: KeyValue) void {
    var key = pair.key.buffer[0..pair.key.index];
    var prefix_length = std.mem.indexOf(u8, key, ".") orelse key.len;
    if (key.len <= (prefix_length + 2)) {
        // invalid key in conf, should have been handled during parsing
        unreachable;
    }
    key = key[prefix_length + 1 ..];
    if (std.mem.eql(u8, key, "symbol")) {
        conf.symbol.assign(pair.value);
    } else {
        // invalid key in conf should have been handled during key validation
        unreachable;
    }
}

fn process_conf_field(conf: *Conf, pair: KeyValue) bool {
    inline for (@typeInfo(Fields).Enum.fields) |f| {
        const expected = f.name ++ ".";
        if (pair.key.starts_with(expected)) {
            parse_field_conf_entry(&conf.fields[f.value], pair);
            return true;
        }
    }
    return false;
}

fn process_conf_gradient(conf: *Conf, pair: KeyValue) bool {
    const expected_key = "gradient.";
    if (!pair.key.starts_with(expected_key)) {
        return false;
    }
    var remainder = pair.key.buffer[expected_key.len..pair.key.index];
    const num = std.fmt.parseFloat(f32, remainder) catch |err| blk: {
        if (std.mem.eql(u8, remainder, "start")) {
            break :blk @as(f32, 0.0);
        } else if (std.mem.eql(u8, remainder, "end")) {
            break :blk 1.0;
        }
        std.debug.print("Unexpected gradient key '{s}' ({})\n", .{ remainder, err });
        break :blk 0.0;
    };
    const color_u32 = eval_unsigned(pair.value.as_slice());
    conf.gradient.insert_color_stop(num, color.rgb_to_color(color_u32)) catch |err| {
        switch (err) {
            error.NotEnoughSpace => {
                std.debug.print("Too many color stops specified\n", .{});
            },
            else => {
                std.debug.print("Error adding color stop: {}\n", .{err});
            },
        }
    };
    return true;
}

pub fn load(out_file: std.fs.File) Conf {
    var buffer: [4096]u8 = undefined;
    var alloc = std.heap.FixedBufferAllocator.init(buffer[0..]);

    var conf_file_data = load_conf(&alloc.allocator);
    defer conf_file_data.deinit();

    var conf = Default;
    for (conf_file_data.items) |pair| {
        if (pair.key.eql("separator")) {
            conf.separator.clear();
            eval_string(&conf.separator, pair.value.buffer[0..pair.value.index]);
        } else if (process_conf_field(&conf, pair)) {
            // nop
        } else if (process_conf_gradient(&conf, pair)) {
            // nop
        } else {
            unreachable;
        }
    }

    conf.encoding = guess_color_encoding(out_file);
    return conf;
}
