const std = @import("std");

const color = @import("color.zig");
const common = @import("common.zig");
const conf = @import("conf.zig");
const cpu = @import("cpu.zig");
const loadavg = @import("loadavg.zig");
const mem = @import("mem.zig");
const mon = @import("mon.zig");
const uptime = @import("uptime.zig");

const Buffer = common.Buffer;
const Color = color.Color;
const Colors = color.Colors;
const ColorDef = color.ColorDef;
const Measurment = common.Measurment;
const Conf = conf.Conf;

pub fn format_measurment(config: Conf, buffer: *Buffer, measurment: Measurment, prev_color: ColorDef) void {
    var bg_color = prev_color;
    var fg_color = measurment.coloring;
    if (buffer.index > 0) {
        // do not show separator before the first measurement
        color.set_colors(config, buffer, fg_color, bg_color);
        buffer.append(config.separator);
    }
    color.set_colors(config, buffer, color.ColorDef{ .color = Colors.White }, fg_color);
    if (measurment.field) |field| {
        var symbol = config.fields[@enumToInt(field)].symbol;
        if (symbol.index > 0) {
            buffer.append(" ");
            buffer.append(symbol);
        }
    }
    buffer.append(" ");
    buffer.append(measurment.text);
    buffer.append(" ");
}

pub fn main() !void {
    const stdout = std.io.getStdOut();

    var socket = mon.spawn_if_needed();
    defer if (socket) |fd| {
        std.os.closeSocket(fd);
    };

    var config = conf.load(stdout);

    var uptime_msr = try uptime.get();
    var loadavg_1min = try loadavg.loadavg_1min();
    var mem_pct_used = try mem.get_pct_used();
    var cpu_pct_used = try cpu.get_pct_used(socket);

    var buffer = Buffer{};
    var prev_color: ColorDef = .{ .none = {} };
    format_measurment(config, &buffer, uptime_msr, prev_color);
    prev_color = uptime_msr.coloring;
    format_measurment(config, &buffer, cpu_pct_used, prev_color);
    prev_color = cpu_pct_used.coloring;
    format_measurment(config, &buffer, mem_pct_used, prev_color);
    prev_color = mem_pct_used.coloring;
    format_measurment(config, &buffer, loadavg_1min, prev_color);

    buffer.append("\n");
    _ = try stdout.write(buffer.buffer[0..buffer.index]);
}
