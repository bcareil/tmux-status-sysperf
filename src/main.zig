const std = @import("std");
const mem = @import("mem.zig");
const loadavg = @import("loadavg.zig");
const common = @import("common.zig");
const mon = @import("mon.zig");

const uptime = "/proc/uptime";
const powerline_gt = "";
const powerline_lt = "";

const Color = common.Color;
const Coloring = common.Coloring;
const Measurment = common.Measurment;

const Colors = struct {
    pub const White: Color = Color{ .r = 1.0, .g = 1.0, .b = 1.0 };
};

const Buffer = struct {
    index: usize = 0,
    buffer: [1024]u8 = undefined,
};

const GradientStart = rgb_to_color(0x3a467d);
const GradientEnd = rgb_to_color(0xfc5444);

const ColorEncoding = enum {
    AnsiEscapeCode,
    Tmux,
};

pub fn rgb_to_color(c: u32) Color {
    const b = c & 0xff;
    const g = (c >> 8) & 0xff;
    const r = (c >> 16) & 0xff;
    return Color{
        .r = @intToFloat(f32, r) / 255.0,
        .g = @intToFloat(f32, g) / 255.0,
        .b = @intToFloat(f32, b) / 255.0,
    };
}

pub fn lerp(a: f32, b: f32, ratio: f32) f32 {
    return a + (b - a) * ratio;
}

pub fn do_gradient(lhs: Color, rhs: Color, ratio: f32) Color {
    return Color{
        .r = lerp(lhs.r, rhs.r, ratio),
        .g = lerp(lhs.g, rhs.g, ratio),
        .b = lerp(lhs.b, rhs.b, ratio),
    };
}

pub fn append_to_buffer(buffer: *Buffer, data: []const u8) void {
    var wrote = for (data) |char, i| {
        if ((i + buffer.index) >= buffer.buffer.len) {
            break i;
        }
        buffer.buffer[buffer.index + i] = char;
    } else blk: {
        break :blk data.len;
    };
    buffer.index += wrote;
}

pub fn append_small_buffer_to_buffer(buffer: *Buffer, data: common.SmallBuffer) void {
    append_to_buffer(buffer, data.buffer[0..data.filled]);
}

pub fn curse_set_color_rgb(buffer: *Buffer, color: Color, code: i32) void {
    var tmp_buffer: [24]u8 = undefined;
    var result = std.fmt.bufPrint(&tmp_buffer, "\x1b[{};2;{};{};{}m", .{
        code,
        @floatToInt(i32, color.r * 255.0),
        @floatToInt(i32, color.g * 255.0),
        @floatToInt(i32, color.b * 255.0),
    });
    if (result) |printed| {
        if (printed.len >= (buffer.buffer.len - buffer.index)) {
            return;
        }
        append_to_buffer(buffer, printed);
    } else |err| {
        std.debug.warn("invalid format: {}\n", .{err});
    }
}

pub fn curse_set_color_bg(buffer: *Buffer, color: Color) void {
    curse_set_color_rgb(buffer, color, 48);
}

pub fn curse_set_color_fg(buffer: *Buffer, color: Color) void {
    curse_set_color_rgb(buffer, color, 38);
}

pub fn curse_set_colors(buffer: *Buffer, fg: ?Color, bg: ?Color) void {
    if (fg) |x_fg| {
        curse_set_color_fg(buffer, x_fg);
    }
    if (bg) |x_bg| {
        curse_set_color_bg(buffer, x_bg);
    }
}

pub fn curse_reset_colors(buffer: *Buffer) void {
    append_to_buffer(buffer, "\x1b[0m");
}

pub fn tmux_set_color(kind: []const u8, buffer: *Buffer, c: Color) void {
    var tmp_buffer: [32]u8 = undefined;
    var printed = std.fmt.bufPrint(&tmp_buffer, "#[{}=#{x:0>2}{x:0>2}{x:0>2}]", .{
        kind,
        @floatToInt(u8, c.r * 255.0),
        @floatToInt(u8, c.g * 255.0),
        @floatToInt(u8, c.b * 255.0),
    }) catch unreachable;
    append_to_buffer(buffer, printed);
}

pub fn tmux_set_colors(buffer: *Buffer, fg: ?Color, bg: ?Color) void {
    if (fg) |c| {
        tmux_set_color("fg", buffer, c);
    }
    if (bg) |c| {
        tmux_set_color("bg", buffer, c);
    }
}

pub fn set_colors(enc: ColorEncoding, buffer: *Buffer, fg: ?Color, bg: ?Color) void {
    switch (enc) {
        .AnsiEscapeCode => {
            curse_set_colors(buffer, fg, bg);
        },
        .Tmux => {
            tmux_set_colors(buffer, fg, bg);
        },
    }
}

pub fn get_uptime(buffer: *Buffer) !void {
    const open_ro = std.fs.File.OpenFlags{ .read = true };
    var uptime_fd = try std.fs.openFileAbsolute(uptime, open_ro);
    defer uptime_fd.close();

    var sub_buffer: []u8 = buffer.buffer[buffer.index..buffer.buffer.len];
    var read = try uptime_fd.read(sub_buffer);
    for (sub_buffer[0..read]) |char, i| {
        if (char == '.') {
            read = i;
            break;
        }
    }
    var seconds: i32 = try std.fmt.parseInt(i32, sub_buffer[0..read], 10);

    const seconds_in_a_min = 60;
    const seconds_in_an_hour = seconds_in_a_min * 60;
    const seconds_in_a_day = seconds_in_an_hour * 24;

    if (seconds > 2 * seconds_in_a_day) {
        var days = @divTrunc(seconds, seconds_in_a_day);
        seconds = @mod(seconds, seconds_in_a_day);
        var printed = try std.fmt.bufPrint(sub_buffer, "{} days ", .{days});
        sub_buffer = sub_buffer[printed.len..];
        buffer.index += printed.len;
    }

    var hours: i32 = 0;
    if (seconds > seconds_in_an_hour) {
        hours = @divTrunc(seconds, seconds_in_an_hour);
        seconds = @mod(seconds, seconds_in_an_hour);
    }
    var minutes = @intCast(u32, @divTrunc(seconds, seconds_in_a_min));
    var printed = try std.fmt.bufPrint(sub_buffer, "{}h{:0>2}", .{ hours, minutes });
    buffer.index += printed.len;
}

pub fn coloring_to_curse_color(c: Coloring) ?Color {
    return switch (c) {
        Coloring.gradient => |x_gradient| do_gradient(GradientStart, GradientEnd, x_gradient),
        Coloring.color => |x_color| x_color,
        Coloring.none => null,
    };
}

pub fn format_measurment(enc: ColorEncoding, buffer: *Buffer, measurment: Measurment, prev_color: Coloring) void {
    var bg_color = coloring_to_curse_color(prev_color);
    var fg_color = coloring_to_curse_color(measurment.coloring);
    set_colors(enc, buffer, fg_color, bg_color);
    append_to_buffer(buffer, powerline_lt);
    set_colors(enc, buffer, Colors.White, fg_color);
    append_to_buffer(buffer, " ");
    append_small_buffer_to_buffer(buffer, measurment.text);
    append_to_buffer(buffer, " ");
}

pub fn guess_color_encoding(stream: std.fs.File.Writer) ColorEncoding {
    return ColorEncoding.Tmux;
}

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const color_enc = guess_color_encoding(stdout);

    var socket = mon.spawn_if_needed();
    defer if (socket) |fd| {
        std.os.closeSocket(fd);
    };

    var buffer = Buffer{};
    try get_uptime(&buffer);
    append_to_buffer(&buffer, " ");

    var loadavg_1min = try loadavg.loadavg_1min();
    var mem_pct_used = try mem.mem_pct_used();

    var cpu_usage = mon.recv_cpu_usage(socket);
    var cpu_pct_used = Measurment{
        .text = common.SmallBuffer{},
        .coloring = common.Coloring{ .gradient = cpu_usage },
    };
    var printed = std.fmt.bufPrint(&cpu_pct_used.text.buffer, "{d:>3.0}%", .{100.0 * cpu_usage}) catch unreachable;
    cpu_pct_used.text.filled = printed.len;

    var prev_color: Coloring = .{ .none = {} };
    format_measurment(color_enc, &buffer, cpu_pct_used, prev_color);
    prev_color = cpu_pct_used.coloring;
    format_measurment(color_enc, &buffer, mem_pct_used, prev_color);
    prev_color = mem_pct_used.coloring;
    format_measurment(color_enc, &buffer, loadavg_1min, prev_color);

    append_to_buffer(&buffer, "\n");
    _ = try stdout.write(buffer.buffer[0..buffer.index]);
}
