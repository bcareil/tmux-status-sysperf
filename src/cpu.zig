const std = @import("std");

const color = @import("color.zig");
const common = @import("common.zig");
const mon = @import("mon.zig");

const stat_file = "/proc/stat";

const CpuSample = struct {
    user: u32 = 0,
    system: u32 = 0,
};

fn skip_spaces(slice: []const u8) []const u8 {
    var i: usize = 0;
    while (i < slice.len and slice[i] == ' ') {
        i += 1;
    }
    return slice[i..];
}

fn skip_integer(slice: []const u8) ![]const u8 {
    var i: usize = 0;
    while (i < slice.len) {
        switch (slice[i]) {
            '0'...'9' => _ = 0,
            ' ' => break,
            else => return error.InvalidCharacter,
        }
        i += 1;
    }
    return slice[i..];
}

fn parse_cpu_line(buffer: []const u8) !CpuSample {
    var buf = buffer[0..];
    if (!std.mem.eql(u8, buf[0..4], "cpu ")) {
        return error.InvalidCharacter;
    }

    buf = buf[4..];
    buf = skip_spaces(buf);

    var to_parse = buf;
    buf = try skip_integer(buf);
    to_parse = to_parse[0..(to_parse.len - buf.len)];
    var cpu_user = try std.fmt.parseUnsigned(u32, to_parse, 10);
    buf = skip_spaces(buf);

    // cpu nice
    buf = try skip_integer(buf);
    buf = skip_spaces(buf);

    to_parse = buf;
    buf = try skip_integer(buf);
    to_parse = to_parse[0..(to_parse.len - buf.len)];
    var cpu_system = try std.fmt.parseUnsigned(u32, to_parse, 10);

    return CpuSample{ .user = cpu_user, .system = cpu_system };
}

pub fn sample_cpu() !CpuSample {
    const ro = std.fs.File.OpenFlags{ .read = true };
    var stream = try std.fs.openFileAbsolute(stat_file, ro);
    defer stream.close();

    var buffer: [128]u8 = undefined;
    var read = try stream.read(&buffer);
    var sample = try parse_cpu_line(buffer[0..read]);
    return sample;
}

fn test_skip_integer(expected: anyerror![]const u8, input: []const u8) !void {
    var result = skip_integer(input);
    if (result) |result_str| {
        if (expected) |expected_str| {
            if (std.mem.eql(u8, expected_str, result_str)) {
                return;
            }
        } else |_| {}
    } else |err| {
        if (expected) |expected_str| {
            return err;
        } else |expected_err| {
            if (err != expected_err) {
                return err;
            }
        }
        return;
    }
    std.debug.warn("Expected '{}', found '{}\n'", .{ expected, result });
    return error.TestFailed;
}

pub fn get_pct_used(socket: ?std.os.socket_t) !common.Measurment {
    var cpu_usage = blk: {
        if (socket) |sck| {
            break :blk mon.recv_cpu_usage(sck) catch 0.0;
        } else {
            break :blk 0.0;
        }
    };
    var cpu_pct_used = common.Measurment{
        .text = common.SmallBuffer{},
        .coloring = color.ColorDef{ .gradient = cpu_usage },
        .field = .cpu,
    };
    var printed = std.fmt.bufPrint(&cpu_pct_used.text.buffer, "{d:>3.0}%", .{100.0 * cpu_usage}) catch unreachable;
    cpu_pct_used.text.index = printed.len;
    return cpu_pct_used;
}

test "skip_integer" {
    try test_skip_integer(error.InvalidCharacter, "abc");
    try test_skip_integer(error.InvalidCharacter, "123abc");
    try test_skip_integer(" abc", " abc");
    try test_skip_integer(" abc", "123 abc");
}
