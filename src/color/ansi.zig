const std = @import("std");
const common = @import("../common.zig");
const color = @import("../color.zig");

const Buffer = common.Buffer;
const Color = color.Color;
const ColorInt = color.ColorInt;

fn set_color_rgb(buffer: *Buffer, c: Color, code: i32) void {
    var tmp_buffer: [24]u8 = undefined;
    const ci = ColorInt.from_color(c);
    var result = std.fmt.bufPrint(&tmp_buffer, "\x1b[{};2;{};{};{}m", .{
        code,
        ci.r,
        ci.g,
        ci.b,
    });
    if (result) |printed| {
        if (printed.len >= (buffer.buffer.len - buffer.index)) {
            return;
        }
        buffer.append(printed);
    } else |err| {
        std.debug.warn("invalid format: {}\n", .{err});
    }
}

pub fn set_color_bg(buffer: *Buffer, c: Color) void {
    set_color_rgb(buffer, c, 48);
}

pub fn set_color_fg(buffer: *Buffer, c: Color) void {
    set_color_rgb(buffer, c, 38);
}

pub fn set_colors(buffer: *Buffer, fg: ?Color, bg: ?Color) void {
    if (fg) |x_fg| {
        set_color_fg(buffer, x_fg);
    }
    if (bg) |x_bg| {
        set_color_bg(buffer, x_bg);
    }
}

pub fn reset_colors(buffer: *Buffer) void {
    buffer.append("\x1b[0m");
}
