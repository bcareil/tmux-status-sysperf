const std = @import("std");
const common = @import("../common.zig");
const color = @import("../color.zig");

const Buffer = common.Buffer;
const Color = color.Color;
const ColorInt = color.ColorInt;

fn tmux_set_color(kind: []const u8, buffer: *Buffer, c: Color) void {
    var tmp_buffer: [32]u8 = undefined;
    const ci = ColorInt.from_color(c);
    var printed = std.fmt.bufPrint(&tmp_buffer, "#[{s}=#{x:0>2}{x:0>2}{x:0>2}]", .{
        kind,
        ci.r,
        ci.g,
        ci.b,
    }) catch unreachable;
    buffer.append(printed);
}

pub fn set_colors(buffer: *Buffer, fg: ?Color, bg: ?Color) void {
    if (fg) |c| {
        tmux_set_color("fg", buffer, c);
    }
    if (bg) |c| {
        tmux_set_color("bg", buffer, c);
    }
}
