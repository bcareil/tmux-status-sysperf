const std = @import("std");
const cpu = @import("cpu.zig");

const fname = "tmux-sysmon.socket";
const path = "/tmp/";
const sample_rate: i64 = 100; // 1 sample every 100ms

const MonError = error{ ForkError, ChildError, CantSpawn, ConnectionError };

const History = struct {
    buffer: [50]u32 = [_]u32{0} ** 50,
    write_index: usize = 0,

    pub fn write(self: *History, value: u32) void {
        self.buffer[self.write_index] = value;
        if (self.write_index == self.buffer.len - 1) {
            self.write_index = 0;
        } else {
            self.write_index += 1;
        }
    }

    pub fn compute_usage(self: History) f32 {
        var idx_prev = self.write_index;
        var idx_last = self.write_index + self.buffer.len - 1;
        idx_last = idx_last % self.buffer.len;
        var prev = self.buffer[idx_prev];
        var last = self.buffer[idx_last];
        var diff = last - prev;
        var usage = 5e-4 * @intToFloat(f32, diff);
        return usage;
    }
};

const UnixSocket = struct {
    addr: std.net.Address,
    fd: std.os.socket_t,
};

fn getuid() usize {
    return asm volatile ("syscall"
        : [ret] "={rax}" (-> usize)
        : [number] "{rax}" (@as(usize, 102)) // linux x86_64
        : "rcx", "r11", "memory"
    );
}

fn mkstemp() !std.fs.File {
    var seed = @intCast(u64, std.time.milliTimestamp());
    var rng = std.rand.DefaultPrng.init(seed);
    var num = rng.random.int(u32);
    var enc = std.base64.standard_encoder;
    comptime var enc_len = std.base64.Base64Encoder.calcSize(4);
    var rand_str: [enc_len]u8 = undefined;
    var num_as_bytes = @bitCast([4]u8, num);
    enc.encode(&rand_str, &num_as_bytes);
    var filename: [64]u8 = undefined;
    var printed = std.fmt.bufPrint(&filename, "/tmp/tmux-status-sysperf-d-{s}.log", .{rand_str}) catch unreachable;
    return std.fs.createFileAbsolute(printed, .{});
}

fn send_cpu_usage(socket: std.os.socket_t, cpu_usage: f32) void {
    var buffer: [4]u8 = @bitCast([4]u8, cpu_usage);
    _ = std.os.sendto(socket, &buffer, std.os.MSG_NOSIGNAL | std.os.MSG_EOR, null, 0) catch {};
}

pub fn recv_cpu_usage(socket: std.os.socket_t) !f32 {
    var buffer = [_]u8{0} ** 4;
    var received = std.os.recv(socket, &buffer, 0);
    if (received) |len| {
        if (len == 4) {
            return @bitCast(f32, buffer);
        }
        return error.UnexpectedMessageLength;
    } else |err| {
        return err;
    }
}

pub fn sample_cpu(history: *History, next_sample: *i64, cpu_usage: *f32) void {
    var ts = std.time.milliTimestamp();
    if (ts < next_sample.*) {
        return;
    }

    next_sample.* += sample_rate;

    var sample = cpu.sample_cpu() catch |err| {
        std.debug.print("Error sampling CPU: {}\n", .{err});
        return;
    };
    history.write(sample.user + sample.system);

    cpu_usage.* = history.compute_usage();
}

fn daemon_main_loop(socket: UnixSocket) void {
    std.debug.print("Daemon started\n", .{});
    var history = History{};
    var next_sample: i64 = std.time.milliTimestamp();
    var cpu_usage: f32 = 0;
    var pollfd = [1]std.os.pollfd{std.os.pollfd{
        .fd = socket.fd,
        .events = std.os.POLLIN,
        .revents = undefined,
    }};
    while (true) {
        var time_to_next_sample = next_sample - std.time.milliTimestamp();
        var timeout = std.math.max(0, @intCast(i32, time_to_next_sample));
        var nevents = std.os.poll(&pollfd, timeout) catch 0;
        // NOTE: nevents not decremented, we expect WouldBlock error to break
        // out of the loop
        while (nevents > 0) {
            var accepted_addr: std.net.Address = undefined;
            var adr_len: std.os.socklen_t = @sizeOf(std.net.Address);
            var result = std.os.accept(socket.fd, &accepted_addr.any, &adr_len, 0);
            if (result) |fd| {
                defer std.os.closeSocket(fd);
                send_cpu_usage(fd, cpu_usage);
            } else |err| switch (err) {
                error.WouldBlock => break,
                else => |e| std.debug.print("Unexpected error {}\n", .{e}),
            }
        }

        sample_cpu(&history, &next_sample, &cpu_usage);
    }
}

fn init_unix_socket(socket_fname: []const u8, blocking: bool) !UnixSocket {
    var flags: u32 = std.os.linux.SOCK_SEQPACKET;
    if (!blocking) {
        flags |= std.os.linux.SOCK_NONBLOCK;
    }
    var socket = std.os.socket(std.os.linux.AF_UNIX, flags, 0) catch {
        std.debug.print("Error: socket", .{});
        return MonError.ChildError;
    };
    errdefer std.os.closeSocket(socket);

    var sock_addr = std.os.sockaddr_un{
        .family = std.os.AF_UNIX,
        .path = undefined,
    };

    std.mem.set(u8, &sock_addr.path, 0);
    if (socket_fname.len > sock_addr.path.len) {
        std.debug.print("Socket file name too long: {} greater than {}", .{ socket_fname.len, sock_addr.path.len });
        return MonError.ChildError;
    }
    std.mem.copy(u8, &sock_addr.path, socket_fname);

    var address = std.net.Address{ .un = sock_addr };
    return UnixSocket{
        .addr = address,
        .fd = socket,
    };
}

fn spawn_daemon(socket_fname: []const u8) !void {
    if (std.os.fork()) |pid| {
        if (pid == 0) {
            // child
            var ret = std.os.linux.syscall0(std.os.linux.SYS.setsid);
            if (ret == -1) {
                std.debug.print("Error: setsid", .{});
                return MonError.ChildError;
            }

            var new_in = std.fs.openFileAbsolute("/dev/null", .{ .read = true }) catch unreachable;
            defer new_in.close();

            var new_out = mkstemp() catch |err| {
                std.debug.print("Error: mkstemp: {}", .{err});
                return MonError.ChildError;
            };
            defer new_out.close();

            _ = std.os.linux.dup2(new_out.handle, 1);
            _ = std.os.linux.dup2(new_out.handle, 2);
            _ = std.os.linux.dup2(new_in.handle, 0);
        } else {
            // parent
            return;
        }
    } else |err| {
        return MonError.ForkError;
    }

    var socket = try init_unix_socket(socket_fname, false);

    // remove the file, ignore if missing
    std.fs.deleteFileAbsolute(socket_fname) catch {};

    var sl = socket.addr.getOsSockLen();
    std.os.bind(socket.fd, &socket.addr.any, sl) catch {
        std.debug.print("Error: bind", .{});
        return MonError.ChildError;
    };

    std.os.listen(socket.fd, 32) catch {
        std.debug.print("Error: listen", .{});
        return MonError.ChildError;
    };

    daemon_main_loop(socket);
    std.os.exit(0);
}

fn connect(socket_fname: []const u8) !std.os.socket_t {
    var socket = try init_unix_socket(socket_fname, true);
    errdefer std.os.closeSocket(socket.fd);

    var sl = socket.addr.getOsSockLen();
    std.os.connect(socket.fd, &socket.addr.any, sl) catch {
        std.debug.print("Error: connect", .{});
        return MonError.ConnectionError;
    };

    return socket.fd;
}

pub fn spawn_if_needed() ?std.os.socket_t {
    var uid = getuid();
    var buffer: [128]u8 = undefined;
    var socket_fname = std.fmt.bufPrint(&buffer, "{s}{}-{s}", .{ path, uid, fname }) catch unreachable;
    if (connect(socket_fname)) |stream| {
        return stream;
    } else |c_err| switch (c_err) {
        error.CantSpawn => _ = 0,
        else => spawn_daemon(socket_fname) catch |err| switch (err) {
            error.ChildError => std.os.exit(0),
            else => std.debug.print("Failed spawning daemon", .{}),
        },
    }
    return null;
}
