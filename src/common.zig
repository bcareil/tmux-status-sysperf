const std = @import("std");

const color = @import("color.zig");
const conf = @import("conf.zig");

fn BufferImpl(comptime size: usize) type {
    return struct {
        const Self = @This();

        index: usize = 0,
        buffer: [size]u8 = undefined,

        pub fn from_slice(data: []const u8) !Self {
            if (data.len > size) {
                return error.NotEnoughSpace;
            }
            var s = Self{};
            s.assign(data);
            return s;
        }

        pub fn as_slice(self: Self) []const u8 {
            return self.buffer[0..self.index];
        }

        pub fn clear(self: *Self) void {
            self.index = 0;
        }

        pub fn assign(self: *Self, data: anytype) void {
            self.index = 0;
            switch (@typeInfo(@TypeOf(data))) {
                .Array => {
                    self.append(data[0..]);
                },
                .Pointer => |info| {
                    switch (info.size) {
                        .One => self.assign(data.*),
                        .Slice => self.append(data[0..]),
                        else => @compileError("Unsupported type"),
                    }
                },
                .Struct => {
                    self.assign(data.buffer[0..data.index]);
                },
                else => {
                    @compileError("Unsupported type");
                },
            }
        }

        pub fn appendSlice(b: *Self, data: []const u8) void {
            var wrote = for (data) |char, i| {
                if ((i + b.index) >= size) {
                    break i;
                }
                b.buffer[b.index + i] = char;
            } else blk: {
                break :blk data.len;
            };
            b.index += wrote;
        }

        pub fn appendBuffer(b: *Self, oth: anytype) void {
            b.append(oth.buffer[0..oth.index]);
        }

        pub fn appendChar(b: *Self, c: u8) void {
            if (b.index < size) {
                b.buffer[b.index] = c;
                b.index += 1;
            }
        }

        pub fn append(self: *Self, data: anytype) void {
            switch (@typeInfo(@TypeOf(data))) {
                .Array => self.appendSlice(data[0..]),
                .Struct => self.appendBuffer(data),
                .Int, .ComptimeInt => self.appendChar(data),
                .Pointer => |info| {
                    switch (info.size) {
                        .One => self.append(data.*),
                        .Slice => self.appendSlice(data),
                        else => @compileError("Unsupported type"),
                    }
                },
                else => @compileError("Unsupported type"),
            }
        }

        fn to_slice(data: anytype) []const u8 {
            switch (@typeInfo(@TypeOf(data))) {
                .Array => {
                    return data[0..];
                },
                .Pointer => |info| {
                    switch (info.size) {
                        .One => return to_slice(data.*),
                        .Slice => return data,
                        else => @compileError("Unsupported Type"),
                    }
                },
                .Struct => {
                    return rhs.buffer[0..rhs.index];
                },
                else => {
                    @compileError("Unsupported type");
                },
            }
        }

        pub fn eql(lhs: Self, rhs: anytype) bool {
            return std.mem.eql(u8, lhs.buffer[0..lhs.index], to_slice(rhs));
        }

        pub fn starts_with(lhs: Self, rhs: anytype) bool {
            const slice = to_slice(rhs);
            if (slice.len <= lhs.index) {
                return std.mem.eql(u8, lhs.buffer[0..slice.len], slice);
            }
            return false;
        }
    };
}

pub const SmallBuffer = BufferImpl(64);

pub const Buffer = BufferImpl(1024);

pub const Measurment = struct {
    text: SmallBuffer,
    coloring: color.ColorDef,
    field: ?conf.Fields,

    pub fn init() Measurment {
        return Measurment{
            .text = SmallBuffer{},
            .coloring = .none,
            .field = null,
        };
    }
};
