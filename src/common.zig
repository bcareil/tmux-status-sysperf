const std = @import("std");

pub const SmallBuffer = struct {
    filled: usize = 0,
    buffer: [64]u8 = undefined,
};

pub const Color = struct {
    r: f32,
    g: f32,
    b: f32,
};

pub const Coloring = union(enum) {
    gradient: f32,
    color: Color,
    none: void,
};

pub const Measurment = struct {
    text: SmallBuffer,
    coloring: Coloring,
};
