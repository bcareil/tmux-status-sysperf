pub fn eql(lhs: anytype, rhs: @TypeOf(lhs)) bool {
    const T = @TypeOf(lhs);

    switch (@typeInfo(T)) {
        .Struct => |info| {
            return lhs.eql(rhs);
        },
        .ErrorUnion => {
            if (lhs) |lhs_ok| {
                if (rhs) |rhs_ok| {
                    return eql(lhs_ok, rhs_ok);
                } else |rhs_err| {
                    return false;
                }
            } else |lhs_err| {
                if (rhs) |rhs_ok| {
                    return false;
                } else |rhs_err| {
                    return lhs_err == rhs_err;
                }
            }
        },
        .Optional => {
            if (lhs == null and rhs == null) {
                return true;
            }
            if (lhs == null or rhs == null) {
                return false;
            }
            return eql(lhs.?, rhs.?);
        },
        else => @compileError("Not implemented"),
    }
}
