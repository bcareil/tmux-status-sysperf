const std = @import("std");

const common = @import("common.zig");

const uptime = "/proc/uptime";

pub fn get() !common.Measurment {
    const open_ro = std.fs.File.OpenFlags{ .read = true };
    var uptime_fd = try std.fs.openFileAbsolute(uptime, open_ro);
    defer uptime_fd.close();

    var buffer: [64]u8 = undefined;
    var read = try uptime_fd.read(&buffer);
    read = std.mem.indexOf(u8, buffer[0..read], ".") orelse read;
    var seconds: i32 = try std.fmt.parseInt(i32, buffer[0..read], 10);

    const seconds_in_a_min = 60;
    const seconds_in_an_hour = seconds_in_a_min * 60;
    const seconds_in_a_day = seconds_in_an_hour * 24;

    var measurment = common.Measurment.init();

    var sub_buffer: []u8 = measurment.text.buffer[0..];
    var buffer_index = &measurment.text.index;
    if (seconds > 2 * seconds_in_a_day) {
        var days = @divTrunc(seconds, seconds_in_a_day);
        seconds = @mod(seconds, seconds_in_a_day);
        var printed = try std.fmt.bufPrint(sub_buffer, "{} days ", .{days});
        sub_buffer = sub_buffer[printed.len..];
        buffer_index.* += printed.len;
    }

    var hours: i32 = 0;
    if (seconds > seconds_in_an_hour) {
        hours = @divTrunc(seconds, seconds_in_an_hour);
        seconds = @mod(seconds, seconds_in_an_hour);
    }
    var minutes = @intCast(u32, @divTrunc(seconds, seconds_in_a_min));
    var printed = try std.fmt.bufPrint(sub_buffer, "{}h{:0>2}", .{ hours, minutes });
    buffer_index.* += printed.len;

    return measurment;
}
