const Builder = @import("std").build.Builder;

pub fn build(b: *Builder) void {
    const mode = b.standardReleaseOptions();

    const exe = b.addExecutable("tmux-status-sysperf", "src/main.zig");
    exe.setBuildMode(mode);
    const run_cmd = exe.run();
    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const test_step = b.step("test", "Run all tests");
    const test_files = [_][]const u8{
        "src/cpu.zig",
        "src/parser.zig",
        "src/color.zig",
    };
    for (test_files) |file| {
        const tests = b.addTest(file);
        tests.setBuildMode(mode);
        test_step.dependOn(&tests.step);
    }

    b.default_step.dependOn(&exe.step);
    b.installArtifact(exe);
}
