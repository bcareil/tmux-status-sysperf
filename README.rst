Disclaimer
==========

Know that this is a toy project. Install and use at your own risks.

Requirements
============

Only available on Linux as of now.

In order for the output to show properly, you will need:

* A full color compatible terminal
* Enable `full color support in tmux`_
* A font supporting `Powerline`_ symbols
* Utf-8 encoding

.. _full color support in tmux: https://github.com/tmux/tmux/wiki/FAQ#how-do-i-use-rgb-colour
.. _Powerline: https://github.com/powerline/fonts

And to compile:

* `zig`_

.. _zig: https://ziglang.org

Installation
============

From the root of the repository::

    zig build -Drelease-safe=true --prefix /where/ever install

Usage
=====

Once it is installed in your PATH::

    set -g status-right "#(tmux-status-sysperf)"


Example
=======

Here is what the commands outputs if run in standalone in Tmux status line:

.. image:: sample.png

In order, you have:

#. Uptime
#. CPU average
#. Memory usage (exclude cache and buffer)
#. Load average (1min)

As show in the previous link, in order to benefits from the full gradient of
colors used by the program, you will need to enable full color support in Tmux.
I also recommend changing the update interval of the status line. All in all, a
full example configuration will look like this::

    set -g default-terminal tmux-256color
    set -as terminal-overrides ",xterm-256color*:Tc"
    set -g status-interval 1
    set -g status-right "#(tmux-status-sysperf)#[fg=#40467b]#[fg=white,bg=#40467b] %R"

Bugs
====

Yes.
